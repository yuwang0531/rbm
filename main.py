import os
import json
import matplotlib.pyplot as plt
from collections import defaultdict
from math import sqrt
import numpy as np
import theano
import theano.tensor as T
import theano.sparse

def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

def avg(x):
    return sum(x)/len(x)

def _expand_line(line, k=5):
    expanded = [0] * (len(line) * k)
    for i, el in enumerate(line):
        if float(el) != 0.:
            el = float(el)
            expanded[(i*k) + int(round(el)) - 1] = 1
    return expanded

def expand(data, k=5):
    new = []
    for m in data:
        new.extend(_expand_line(m.tolist()))
    return np.array(new).reshape(data.shape[0], data.shape[1] * k)

def revert_expected_value(m, k=5, do_round=True):
    mask = list(range(1, k+1))
    vround = np.vectorize(round)
    if do_round:
        users = vround((m.reshape(-1, k) * mask).sum(axis=1))
    else:
        users = (m.reshape(-1, k) * mask).sum(axis=1)
    return np.array(users).reshape(m.shape[0], m.shape[1] / k)


def load_dataset(train_path, tune_path, test_path, sep):
    all_users_set = set()
    all_movies_set = set()
    trains = defaultdict(list)
    tunes = defaultdict(list)    
    tests = defaultdict(list)
    with open(train_path, 'rt') as data:
        for i, line in enumerate(data):
            uid, mid, rat = line.strip().split(sep)
            trains[uid].append((mid, float(rat)))
            if uid not in all_users_set:
                all_users_set.add(uid)
            if mid not in all_movies_set:
                all_movies_set.add(mid)    
    with open(tune_path, 'rt') as data:
        for i, line in enumerate(data):
            uid, mid, rat = line.strip().split(sep)
            tunes[uid].append((mid, float(rat)))
            if uid not in all_users_set:
                all_users_set.add(uid)
            if mid not in all_movies_set:
                all_movies_set.add(mid)
    with open(test_path, 'rt') as data:
        for i, line in enumerate(data):
            uid, mid, rat = line.strip().split(sep)
            tests[uid].append((mid, float(rat)))
            if uid not in all_users_set:
                all_users_set.add(uid)
            if mid not in all_movies_set:
                all_movies_set.add(mid)
    return trains, tests, tunes, list(all_users_set), list(all_movies_set)

def outer(x, y):
    return x[:, :, np.newaxis] * y[:, np.newaxis, :]

def cast32(x):
    return T.cast(x, 'float32')

class CFRBM:
    def __init__(self, num_visible, num_hidden, initial_v=None,initial_weigths=None):
        self.dim = (num_visible, num_hidden)
        self.num_visible = num_visible
        self.num_hidden = num_hidden
        if initial_weigths:
            initial_weights = np.load('{}.W.npy'.format(initial_weigths))
            initial_hbias = np.load('{}.h.npy'.format(initial_weigths))
            initial_vbias = np.load('{}.b.npy'.format(initial_weigths))
        else:
            initial_weights = np.array(np.random.normal(0, 0.1, size=self.dim),dtype=np.float32)
            initial_hbias = np.zeros(num_hidden, dtype=np.float32)
            if initial_v:
                initial_vbias = np.array(initial_v, dtype=np.float32)
            else:
                initial_vbias = np.zeros(num_visible, dtype=np.float32)
        self.weights = theano.shared(value=initial_weights, borrow=True, name='weights')
        self.vbias = theano.shared(value=initial_vbias, borrow=True, name='vbias')
        self.hbias = theano.shared(value=initial_hbias, borrow=True, name='hbias')
        prev_gw = np.zeros(shape=self.dim, dtype=np.float32)
        self.prev_gw = theano.shared(value=prev_gw, borrow=True, name='g_w')
        prev_gh = np.zeros(num_hidden, dtype=np.float32)
        self.prev_gh = theano.shared(value=prev_gh, borrow=True, name='g_h')
        prev_gv = np.zeros(num_visible, dtype=np.float32)
        self.prev_gv = theano.shared(value=prev_gv, borrow=True, name='g_v')
        self.theano_rng = T.shared_randomstreams.RandomStreams(np.random.RandomState(17).randint(2**30))

    def prop_up(self, vis):
        return T.nnet.sigmoid(T.dot(vis, self.weights) + self.hbias)

    def sample_hidden(self, vis):
        activations = self.prop_up(vis)
        h1_sample = self.theano_rng.binomial(size=activations.shape, n=1, p=activations, dtype=theano.config.floatX)
        return h1_sample, activations

    def prop_down(self, h):
        return T.nnet.sigmoid(T.dot(h, self.weights.T) + self.vbias)

    def sample_visible(self, h, k=5):
        activations = self.prop_down(h)
        k_ones = T.ones(k)
        partitions = activations.reshape((-1, k)).sum(axis=1).reshape((-1, 1)) * k_ones
        activations = activations / partitions.reshape(activations.shape)
        v1_sample = self.theano_rng.binomial(size=activations.shape, n=1, p=activations, dtype=theano.config.floatX)
        return v1_sample, activations

    def contrastive_divergence_1(self, v1):
        h1, _ = self.sample_hidden(v1)
        v2, v2a = self.sample_visible(h1)
        h2, h2a = self.sample_hidden(v2)
        return (v1, h1, v2, v2a, h2, h2a)

    def gradient(self, v1, h1, v2, h2p, masks):
        v1h1_mask = outer(masks, h1)
        gw = ((outer(v1, h1) * v1h1_mask) - (outer(v2, h2p) * v1h1_mask)).mean(axis=0)
        gv = ((v1 * masks) - (v2 * masks)).mean(axis=0)
        gh = (h1 - h2p).mean(axis=0)
        return (gw, gv, gh)

    def cdk_fun(self, vis, masks, k=1, w_lr=0.000021, v_lr=0.000025, h_lr=0.000025, decay=0.0000, momentum=0.0):
        v1, h1, v2, v2a, h2, h2a = self.contrastive_divergence_1(vis)
        for i in range(k-1):
            v1, h1, v2, v2a, h2, h2a = self.contrastive_divergence_1(v2)
        (W, V, H) = self.gradient(v1, h1, v2, h2a, masks)
        if decay:
            W -= decay * self.weights
        updates = [
            (self.weights, cast32(self.weights + (momentum * self.prev_gw) + (W * w_lr))),
            (self.vbias, cast32(self.vbias + (momentum * self.prev_gv) + (V * v_lr))),
            (self.hbias, cast32(self.hbias + (momentum * self.prev_gh) + (H * h_lr))),
            (self.prev_gw, cast32(W)),
            (self.prev_gh, cast32(H)),
            (self.prev_gv, cast32(V))]
        return theano.function([vis, masks], updates=updates)

    def predict(self, v1):
        h1, _ = self.sample_hidden(v1)
        v2, v2a = self.sample_visible(h1)
        return theano.function([v1], v2a)

def run(profiles, tunes, tests, all_users, all_movies, initial_v, sep):
    number_hidden = 200
    epochs = 70
    ks = [1]
    momentums = [0.5]
    l_w = [0.0005]
    l_v = [0.0005]
    l_h = [0.0005]
    decay = 0.0002
    batch_size = 40
    vis = T.matrix()
    vmasks = T.matrix()
    rbm = CFRBM(len(all_movies)*5, number_hidden)
    rmse_tune = []
    rmse_test = []
    for j in range(epochs):
        def get_index(col):
            if j/(epochs/len(col)) < len(col):
                return int(j/(epochs/len(col)))
            else:
                return -1
        index = get_index(ks)
        mindex = get_index(momentums)
        icurrent_l_w = get_index(l_w)
        icurrent_l_v = get_index(l_v)
        icurrent_l_h = get_index(l_h)
        k = ks[index]
        momentum = momentums[mindex]
        current_l_w = l_w[icurrent_l_w]
        current_l_v = l_v[icurrent_l_v]
        current_l_h = l_h[icurrent_l_h]
        train = rbm.cdk_fun(vis, vmasks, k=k, w_lr=current_l_w, v_lr=current_l_v, h_lr=current_l_h, decay=decay, momentum=momentum)
        predict = rbm.predict(vis)
        for batch_i, batch in enumerate(chunker(list(profiles.keys()),batch_size)):
            size = min(len(batch), batch_size)
            bin_profiles = {}
            masks = {}
            for userid in batch:
                user_profile = [0.] * len(all_movies)
                mask = [0] * (len(all_movies) * 5)
                for movie_id, rat in profiles[userid]:
                    user_profile[all_movies.index(movie_id)] = rat
                    for _i in range(5):
                        mask[5 * all_movies.index(movie_id) + _i] = 1
                example = expand(np.array([user_profile])).astype('float32')
                bin_profiles[userid] = example
                masks[userid] = mask
            profile_batch = [bin_profiles[id] for id in batch]
            masks_batch = [masks[id] for id in batch]
            train_batch = np.array(profile_batch).reshape(size, len(all_movies) * 5)
            train_masks = np.array(masks_batch).reshape(size, len(all_movies) * 5)
            train_masks = train_masks.astype('float32')
            train(train_batch, train_masks)
            
        ratings_tune = []
        predictions_tune = []
        for batch in chunker(list(tunes.keys()), batch_size):
            size = min(len(batch), batch_size)
            bin_profiles = {}
            masks = {}
            for userid in batch:
                user_profile = [0.] * len(all_movies)
                mask = [0] * (len(all_movies) * 5)
                for movie_id, rat in profiles[userid]:
                    user_profile[all_movies.index(movie_id)] = rat
                    for _i in range(5):
                        mask[5 * all_movies.index(movie_id) + _i] = 1
                example = expand(np.array([user_profile])).astype('float32')
                bin_profiles[userid] = example
                masks[userid] = mask
            positions = {profile_id: pos for pos, profile_id in enumerate(batch)}
            profile_batch = [bin_profiles[el] for el in batch]
            tune_batch = np.array(profile_batch).reshape(size, len(all_movies) * 5)
            user_preds = revert_expected_value(predict(tune_batch))
            for profile_id in batch:
                tune_movies = tunes[profile_id]
                try:
                    for movie, rating in tune_movies:
                        current_profile = user_preds[positions[profile_id]]
                        predicted = current_profile[all_movies.index(movie)]
                        rating = float(rating)
                        ratings_tune.append(rating)
                        predictions_tune.append(predicted)
                except Exception:
                    pass
        vabs = np.vectorize(abs)
        distances = np.array(ratings_tune) - np.array(predictions_tune)
        rmse_tune.append(sqrt((distances ** 2).mean()))

        ratings_test = []
        predictions_test = []
        for batch in chunker(list(tests.keys()), batch_size):
            size = min(len(batch), batch_size)
            bin_profiles = {}
            masks = {}
            for userid in batch:
                user_profile = [0.] * len(all_movies)
                mask = [0] * (len(all_movies) * 5)
                for movie_id, rat in profiles[userid]:
                    user_profile[all_movies.index(movie_id)] = rat
                    for _i in range(5):
                        mask[5 * all_movies.index(movie_id) + _i] = 1
                example = expand(np.array([user_profile])).astype('float32')
                bin_profiles[userid] = example
                masks[userid] = mask
            positions = {profile_id: pos for pos, profile_id in enumerate(batch)}
            profile_batch = [bin_profiles[el] for el in batch]
            test_batch = np.array(profile_batch).reshape(size, len(all_movies) * 5)
            user_preds = revert_expected_value(predict(test_batch))
            for profile_id in batch:
                test_movies = tests[profile_id]
                try:
                    for movie, rating in test_movies:
                        current_profile = user_preds[positions[profile_id]]
                        predicted = current_profile[all_movies.index(movie)]
                        rating = float(rating)
                        ratings_test.append(rating)
                        predictions_test.append(predicted)
                except Exception:
                    pass
        distances = np.array(ratings_test) - np.array(predictions_test)
        rmse_test.append(sqrt((distances ** 2).mean()))

    result = {
        'iteration': j,
        'k': k,
        'momentum': momentum,
        'rmse_tune': rmse_tune,
        'rmse_test': rmse_test,
        'lrate': current_l_w
    }
    with open('number_hidden={}, epochs={}.json'.format(number_hidden,epochs), 'wt') as res_output:
        res_output.write(json.dumps(result, indent=4))
        
    plt.plot(range(1,epochs+1), rmse_tune, marker='o', label='Tuning')
    plt.plot(range(1,epochs+1), rmse_test, marker='.', label='Testing')
    plt.xlabel('Iteration')
    plt.ylabel('RMSE')
    plt.grid(True)
    plt.savefig('number_hidden={}, rbm.png'.format(number_hidden))


train_path =  os.getcwd()+'\\train'
tune_path =  os.getcwd()+'\\tune'
test_path =  os.getcwd()+'\\test'
sep = ','
trains, tests, tunes, all_users, all_movies = load_dataset(train_path, tune_path, test_path, sep)
run(trains, tunes, tests, all_users, all_movies, None, sep)
